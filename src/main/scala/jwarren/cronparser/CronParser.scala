package jwarren.cronparser 

import scalaz._, Scalaz._

object CronParser {
  case class Command(time :List[TimeElemParser.TimeElem], cmd:String);

  // List with all the possible valid times
  // In the same type as the result
  val spec : List[TimeElemParser.TimeElem] = List(
    ("minutes", Range(0, 60)), 
    ("hours", Range(0, 24)), 
    ("day of month", Range(1, 32)), 
    ("month", Range(0, 12)),
    ("day of week", Range(1, 8))
  ).map((TimeElemParser.TimeElem.apply _).tupled);

  def parse(cronString :String) : Either[String, Command] = {
    // split on whitespace
    val parts = cronString.split("\\s+")
    if(parts.length < spec.length + 1){
      return Left("Insufficient elements for a cron string, " ++ 
        s"we need at least ${spec.length + 1}, got ${parts.length}")
    }
    // parse each of the time separately
    val partsPlusSpec = spec.zip(parts)
    val times = partsPlusSpec.map((TimeElemParser.parse _).tupled)

    // merge the end of the parts list back into one command string 
    val cmdStr = parts.drop(spec.length).mkString(" "); 

    // If parsing some of the times has failed (is a Left)
    // return the first Left(error) in the list
    // else return a Right[Command] with the result
    for {
      timesList <- times.sequenceU
    } yield Command(timesList, cmdStr)
  }
}
