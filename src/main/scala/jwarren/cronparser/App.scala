package jwarren.cronparser 
object App {
  def main(args: Array[String]): Unit = {
    CronParser.parse(args.mkString(" ")) match {
      case Left(err) => println(err)
      case Right(cmd) => {
        println(s"command ${cmd.cmd}")
        cmd.time.foreach { t => 
          println(s"${t.name} ${t.times.mkString(" ")}")
        }
      }
    }
  }
}

