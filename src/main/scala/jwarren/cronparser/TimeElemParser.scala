package jwarren.cronparser

import scalaz._, Scalaz._

object TimeElemParser {
  case class TimeElem(name :String, times :Seq[Int]);

  // combine the values in two TimeElems
  private def mergeTimes(a :TimeElem, b :TimeElem) :TimeElem ={
    val mergedValues = (a.times.toSet `union` b.times.toSet)
    new TimeElem(a.name, mergedValues.toSeq)
  }

  // sort the values in a TimeElem
  private def sortTimes(a :TimeElem) :TimeElem = {
    new TimeElem(a.name, a.times.sorted)
  }

  // parse an integer, limited to values in the spec, 
  // return Left(error) if parsing fails
  private def parseValue(spec :TimeElem, value :String) : Either[String, Int] = {
    try {
      val res = value.toInt;
      if( spec.times contains res ){
        return Right(res);
      }
      return Left(s"Out of bounds in ${spec.name}, $value")
    }
    catch {
      case e :NumberFormatException => 
        Left(s"Invalid value in ${spec.name}: $value")
    }
  }
  // parse either a single character or a wildcard, 
  // wildcard returns all possible values
  private def parseSingleton(spec :TimeElem, elem :String) :Either[String, TimeElem] = {
    elem match {
      case "*" => Right(spec)
      case a => for {
           vals <- parseValue(spec, elem)
        } yield new TimeElem(spec.name, List(vals));
    }
  }

  // parse a statement which may be of the form n-m in 
  // (a range, values n-m inclusive)
  // else treat the statement as a single value
  private def parseRange(spec :TimeElem, elem :String) :Either[String, TimeElem] = {
    elem.split("-") match {
      case Array(a) => parseSingleton(spec, elem)
      case Array(s, e) => for {
          start <- parseValue(spec, s).right
          end <- parseValue(spec, e).right
        } yield new TimeElem(spec.name, Range(start, end+1))
    }
  }
  // Parse a statement of the form "*/14" 
  // where * may be either a literal *, a range, or a single value
  // and the /14 may be missing, but if present, 
  // means only values divisible by 14 should be returned
  private def parseDiv(spec :TimeElem, elem :String) :Either[String, TimeElem] = {
    elem.split("/") match {
      case Array(a) => parseRange(spec, a)
      case Array(a, b) => for {
          candidates <- parseRange(spec, a)
          modulus <- parseValue(spec, b)
        } yield new TimeElem(spec.name, candidates.times.filter(_ % modulus == 0))
      case default => Left(s"Too many / characters found in ${spec.name}: $elem")
    }
  }

  def parse(spec :TimeElem, elem :String) :Either[String, TimeElem] = {
    //converting to a list is required to use sequenceU
    val commaSplit = elem.split(',').toList; 
    val parsedList = commaSplit.map(parseDiv(spec, _));
    // If any of the list elements haven't parsed, return a Left[String] with the error
    // else merge the results into one Right[TimeElem]
    // sort them, then return the result 
    for {
      listOfLists <- parsedList.sequenceU
      unsortedValues = listOfLists.reduce(mergeTimes _)
      sortedValues = sortTimes(unsortedValues)
    } yield sortedValues
  }
}
