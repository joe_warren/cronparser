import org.scalatest._

import jwarren.cronparser.CronParser;

class CronParserSpec extends FlatSpec 
    with Matchers 
    with EitherValues
    with GivenWhenThen {

  "A CronParser" should "parse a five time elem values followed by a command" in {
    Given("a string with a value with five valid time elem values, followed by a command");
    val argument = "1 2 3 4 5 foo";

    When("that argument is parsed")
    val result = CronParser.parse(argument);

    Then("the parse should be sucessfull, and contain the command")
    result.right.value.cmd should be ("foo");
    And("the parse should contain time elements")
    result.right.value.time.flatMap(_.times) should contain inOrderOnly (1, 2, 3, 4, 5)
    And("the time elements should have descriptive names")
    result.right.value.time.map(_.name) should contain inOrderOnly ("minutes", "hours", "day of month", "month", "day of week")
  }
  it should "be more or less insensitive to whitespace" in {
    Given("a cron string where the whitespace separators vary");
    val argument = "1   2\t3 4\n5\r\nfoo";

    When("that argument is parsed");
    val result = CronParser.parse(argument);

    Then("the parse should be sucessfull, as if the whitespace was simple")
    result.right.value.cmd should be ("foo");
    result.right.value.time.flatMap(_.times) should contain inOrderOnly (1, 2, 3, 4, 5)
  }
  it should "return a left if the parse fails" in {
    Given("an invalid cron string, with two few components");
    val argument = "1 2";

    When("that argument is parsed");
    val result = CronParser.parse(argument);

    Then("the parse should be fail, returning a left value")
    result should be ('left)
  }
  it should "return a left if any of the time elem parses fail" in {
    Given("an invalid cron string, with a bad time elem value");
    val argument = "1 2 a 4 5 foo";

    When("that argument is parsed");
    val result = CronParser.parse(argument);

    Then("the parse should be fail, returning a left value")
    result should be ('left)
  }

  
}
