import org.scalatest._

import jwarren.cronparser.TimeElemParser;

class TimeElemParserSpec extends FlatSpec 
    with Matchers 
    with EitherValues
    with GivenWhenThen {

  val testSpec = TimeElemParser.TimeElem("the name", Range(0, 10));

  "A TimeElemParser" should "parse a literal integer into a value if it matches the spec" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string with a value within the spec");
    val argument = "5";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should be sucessfull, and return the literal value only")
    result.right.value.times should contain only (5);
  }

  it should "fail to parse a value from outside the spec" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string with a value out of the spec");
    val argument = "15";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should be fail")
    result should be ('left);
  }
  it should "interpret a * character as all values in the spec" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string with the value *");
    val argument = "*";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should be sucessfull, and return all values in the spec")
    result.right.value.times should contain only (0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
  }
  it should "interpret a - character as a range" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string representing a range");
    val argument = "2-4";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should be sucessfull, and return all values in the range")
    result.right.value.times should contain only (2, 3, 4);
  }
  it should "only allow range characters in the spec" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string representing a range with values outside the spec");
    val argument = "2-10";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should fail")
    result should be ('left);
  }
  it should "allow filtering values based on whether they're divisible" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string representing all values divisible by 3");
    val argument = "*/3";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should return all values in the spec divisible by 3")
    result.right.value.times should contain only (0, 3, 6, 9);
  }
  it should "allow filtering a range of values" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string representing all values between 3 and 7 divisible by 2");
    val argument = "3-7/2";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should return all values in the range divisible by 2")
    result.right.value.times should contain only (4, 6);
  }
  it should "allow combining many sets of values in a list" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string representing a list of values and ranges");
    val argument = "1,3-5,7";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should return all values in the list")
    result.right.value.times should contain only (1, 3, 4, 5, 7);
  }
  it should "not return duplicate values, if two ranges overlap" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string representing overlapping ranges");
    val argument = "2-5,4-7";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should not return duplicate values")
    result.right.value.times should contain only (2, 3, 4, 5, 6, 7);
  }
  it should "return values in order" in {
    Given("a specification in the form of a TimeElem")
    val spec = testSpec;
    And("a string representing values out of order");
    val argument = "7,2";

    When("that argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should return the values in order")
    result.right.value.times should contain inOrderOnly (2, 7);
  }
  it should "preserve the name from the spec" in {
    Given("a specification in the form of a TimeElem with a specific name")
    val spec = testSpec;

    val argument = "7,2";
    When("an argument is parsed")
    val result = TimeElemParser.parse(testSpec, argument);

    Then("the parse should return the same name as in the spec")
    result.right.value.name should be ("the name");
  }
}
