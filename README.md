CronParser
==========

A simple cron parser, writen in scala, using scalaz, ScalaTest and SBT.

Compile: 

`sbt compile`

Test:

`sbt test`

Run:

`sbt "run */15 0 1,15 * 1-5 /usr/bin/find"`
