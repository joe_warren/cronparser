name := "cron-parser"
scalaVersion :="2.11.2"
version :="1.0"
libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.21"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

